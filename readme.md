[<p align="center"><img src='https://github.com/brillout/reframe/raw/master/docs/logo/logo-with-title.svg?sanitize=true' width=450 height=94 style="max-width:100%;" alt="Reframe"/></p>](https://github.com/brillout/reframe)
<div><p align="center">
        Framework to create React web apps.
</p></div>
<div><p align="center">
    <b>Easy</b>
    -
    Create apps with page configs.
    &nbsp;&nbsp;&nbsp;
    <b>Universal</b>
    -
    Create all kinds of apps.
    &nbsp;&nbsp;&nbsp;
    <b>Customizable</b>
    -
    Everything is adaptable.
</p></div>
<br/>


# **Repository has moved to GitHub: https://github.com/brillout/reframe.**
